package com.podoinikovi.java.cdi.javase;

public interface Text {
    String getText(String sourceText);
}
