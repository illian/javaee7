package com.podoinikovi.java.cdi.javase;

public class UpperCaseText implements Text {
    @Override
    public String getText(String sourceText) {
        return sourceText.toUpperCase();
    }
}
