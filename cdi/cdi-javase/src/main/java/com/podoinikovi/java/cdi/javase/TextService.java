package com.podoinikovi.java.cdi.javase;

import javax.inject.Inject;

public class TextService {
    @Inject
    private Text text;

    public String writeMessage() {
        return text.getText("Hello, World!");
    }
}
