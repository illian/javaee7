package com.podoinikovi.java.cdi.javase;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.spi.BeanManager;

public class Main {
    private final static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();

        TextService textService = container.instance().select(TextService.class).get();
        LOGGER.info(textService.writeMessage());

        BeanManager manager = container.getBeanManager();
        System.out.println(manager.getBeans(Text.class).size());
        weld.shutdown();
    }
}
